setInterval(pokazCzas, 1000);

let zegar = document.getElementById('zegar');

function pokazCzas(){
    let data = new Date();

    let h = data.getHours  ();
    let m = data.getMinutes();
    let s = data.getSeconds();

    h = h.toString().padStart(2, 0);
    m = m.toString().padStart(2, 0);
    s = s.toString().padStart(2, 0);

    zegar.innerHTML = `${h}:${m}:${s}`;
}

pokazCzas();